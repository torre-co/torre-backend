/**
 * Copyright (C) 2021 Reinier Millo Sánchez
 * All Rights Reserved
 * Author: Reinier Millo Sánchez <reinier.millo88@gmail.com>
 *
 * This file is part of the Torre.co Test Backend API
 * It can't be copied and/or distributed without the express
 * permission of the author.
 */
import "module-alias/register";
import { ClusterServer } from "@ikoabo/server";

import UserRoutes from "@/routers/v1/user.router";

/* Initialize cluster server */
const clusterServer = ClusterServer.setup();

/* Run cluster with base routes */
clusterServer.run({
  "/v1/user": UserRoutes
});
