/**
 * Copyright (C) 2021 Reinier Millo Sánchez
 * All Rights Reserved
 * Author: Reinier Millo Sánchez <reinier.millo88@gmail.com>
 *
 * This file is part of the Torre.co Test Backend API
 * It can't be copied and/or distributed without the express
 * permission of the author.
 */
import { ResponseHandler } from "@ikoabo/server";
import { Router, Request, Response, NextFunction } from "express";
import axios, { AxiosResponse } from "axios";

/* Create router object */
const router = Router();

router.get(
  "/:username",
  (req: Request, res: Response, next: NextFunction) => {
    axios
      .get(`https://bio.torre.co/api/bios/${req.params["username"]}`)
      .then((response: AxiosResponse) => {
        res.locals["response"] = response.data;
        next();
      })
      .catch(next);
  },
  ResponseHandler.error,
  ResponseHandler.success
);

export default router;
