module.exports = {
  apps: [
    {
      name: "TORRE-BACKEND",
      script: "dist/index.js",
      autorestart: true,
      watch: false,
      env: {
        NODE_ENV: "production",
        ENV: "production",
        PORT: 8000,
        INSTANCES: 2,
        LOG: "debug"
      }
    }
  ]
};
