FROM node:14.15.4-buster

# Install MongoDB database
RUN wget -q https://www.mongodb.org/static/pgp/server-4.4.asc -O server-4.4.asc
RUN apt-key add server-4.4.asc
RUN echo "deb http://repo.mongodb.org/apt/debian buster/mongodb-org/4.4 main" > /etc/apt/sources.list.d/mongodb-org-4.4.list
RUN apt-get update
RUN apt-get install -y mongodb-org
#RUN systemctl enable mongod

# Copy source code
WORKDIR /usr/src/app
RUN mkdir /usr/src/app/dist
COPY dist/* /usr/src/app/dist
COPY package.json /usr/src/app
COPY ecosystem.config.js /usr/src/app

# Install app dependencies
ENV NPM_CONFIG_LOGLEVEL warn
RUN npm install --production

# Install pm2 monitor
RUN npm install -g pm2
RUN pm2 startup
RUN ls -l /usr/src/app/dist

# Expose service to the external
EXPOSE 8000

# Run service with pm2 monitor
CMD [ "npm", "run", "start"]
