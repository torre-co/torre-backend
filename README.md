=== Torre.co Backend API Integration

## Installation of the service

- Clone the repository

```
git clone https://gitlab.com/torre-co/torre-backend.git
```

- Install dependencies

```
cd torre-backend
npm install
```

- Build the project

```
npm run build
```

## Running the service

The service can be runned directly from the command line with:

```bash
npm run start
```

or it can be build and runned watching for file changes

```
npm run dev
```

To run the service there are some environment variables that can be used to configure it:

- `LOG`: Set the vebose level of the service debugger, allowed values are: error, warn, info, http, verbose, debug, silly (Default: error)
- `PORT`: Set the running port for the HTTP server (Default: 3000)
- `INTERFACE`: Set the HTTP server listening interface (Default: 127.0.0.1)
- `ENV`: Set the service running mode, allowd values are: dev, production (Default: dev)
- `INSTANCES`: Set the number o workers runing into the cluster (Default: 1)

## Deploy on server

### Run as service

To allow the microservice to run as system service, first you must install `pm2`:

```
npm i -g pm2
```

After that, you must create the ecosystem file to launch the service:

```
nano ecosystem.config.js
```

The `ecosystem.config.js` file contains the following lines:

```
module.exports = {
  apps : [{
    name: 'TORRE-API',
    script: 'dist/index.js',
    autorestart: true,
    watch: false,
    env: {
      NODE_ENV: 'development',
      ENV: 'dev',
      PORT: 3000,
      INSTANCES: 2,
      LOG: 'debug'
    },
  }],
};

```

To start the service run the folowing lines:

```
pm2 start ecosystem.config.js
pm2 save
```

Now the accounts microservice is running as system service.

### Configure Nginx web server

To allow access the service from external,you must configure a new virtual host in the Nginx server:

```
nano /etc/nginx/sites-availables/api.torre.co
```

With the following code:

```
upstream mod-torre-api {
  server localhost:8000;
}

server {
  listen 80;
  listen [::]:80;
  server_name api.torre.co;

  location / {
    proxy_pass http://mod-torre-api/;
    proxy_http_version 1.1;
    proxy_cache_bypass $http_upgrade;
    proxy_read_timeout 600s;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_set_header X-Caller-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
  }
}
```

And then enable the virtual host:

```
ln -s /etc/nginx/sites-available/api.torre.co /etc/nginx/sites-enabled/api.torre.co
```

To allow secure access to the service, use Let's Encrypt certificates. Certificates can be installed with the `certbot` tool:

```
certbot --nginx -d api.torre.co
```

Now you can restart the Nginx server and test the microservice.
